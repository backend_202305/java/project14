package quiz;

/*
 * 다음 코드를 실행하면 에러가 발생합니다.
 * 프로그램이 정상적으로 종료되도록 예외를 처리하세요.
 * */
public class Quiz4 {

	public static void main(String[] args) {
		
		String str= "1.23";
		int num= Integer.parseInt(str); // "1.23"을 정수로 변환할때 Exception 발생
		
//		try {
//			String str= "1.23";
//			int num= Integer.parseInt(str);
//		}catch (NumberFormatException e) {
//			System.out.println(e);
//		}

	}

}
